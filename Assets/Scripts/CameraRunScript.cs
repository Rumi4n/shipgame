﻿using UnityEngine;
using System.Collections;

public class CameraRunScript : MonoBehaviour {

	public float cameraSpeed;

	public float yPosition = 0;

	// Update is called once per frame
	void Update () 
	{
		yPosition += cameraSpeed;
		transform.position = new Vector3(0, yPosition , -10);	
	}
}
