﻿using UnityEngine;
using System.Collections;

public class BgScroller : MonoBehaviour {

	public float speed;
	public static BgScroller current;

	public float pos;

	// Use this for initialization
	void Start () {
		current = this;
	}
	
	// Update is called once per frame
	void Update () {
		pos += speed;

		if (pos > 1.0f) {
			pos -= 1.0f;
		}

		renderer.material.mainTextureOffset = new Vector2 (0, pos);
	}
}
